# Tmux

Yet, another tmux configuration.

## Usage

### Install

    git clone --recurse-submodules git@gitlab.com:pcoves/tmux.git
    stow --target ~ tmux

### Uninstall

    stow --delete --target ~ tmux
    rm -rf tmux

### Plugins

This configuration makes use of [`tmux` plugin manager](https://github.com/tmux-plugins/tpm).
Be sure to follow instructions there.

* `C-I` to install plugins
* `C-U` to update plugins
* `C-u` to uninstall plugins
